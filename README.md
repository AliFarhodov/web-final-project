# Meeting Event Website

- This is the repository for the Meeting Event website, a platform designed to manage and showcase details for conferences and meetings. 
- The website provides information about the event schedule, speakers, venue, hotels, sponsors, and more.

## Table of Contents

- Introduction
- Features
- Installation
- Usage
- Technologies Used
- Contributing
- License
- 
## Features

- Event Information: Find details about the event, including date, location, and a brief overview.
- Speakers: Learn about the featured speakers with their bios and social media links.
- Event Schedule: View the detailed schedule for each day of the event.
- Venue: Explore the event venue through an interactive map and gallery.
- Hotels: Discover nearby hotels for accommodation during the event.
- Gallery: Browse through a gallery of images from previous events.
- Sponsors: Acknowledge and appreciate the sponsors who support the event.
- F.A.Q.: Find answers to frequently asked questions about the event.
- Newsletter: Subscribe to the newsletter for regular updates and announcements.
- Contact Us: Reach out to the event organizers for inquiries and assistance.


## Installation

- To set up the Meeting Event website locally, follow these steps:

````
https://gitlab.com/AliFarhodov/web-final-project.git
````

- Open the index.html file in your preferred web browser.



## Usage

- Navigate through the website using the navigation menu to access different sections. 
- Explore details about the event, speakers, schedule, venue, hotels, gallery, sponsors, and more. Subscribe to the newsletter for regular updates and announcements.
## Technologies Used

- HTML
- CSS
- JavaScript
- jQuery
- Bootstrap
- Owl Carousel
- Venobox
- WOW.js
***

# Contributing

- If you want to contribute to the development of the Meeting Event website, please follow the contribution guidelines.

# License

This project is licensed under the MIT License.





